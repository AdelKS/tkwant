.. _extensions:

Extensions
==========

Additional packages to extend the functionality of Tkwant. Extensions must be
installed separately.

.. toctree::
    :maxdepth: 1

    tkwantOperator - Operators for energy and heat transport <https://kwant-project.org/extensions/tkwantoperator/>


