:mod:`tkwant.system` -- Tools for time-dependent and open Kwant systems
=======================================================================

.. module:: tkwant.system
.. autosummary::
    :toctree: generated

    hamiltonian_with_boundaries
    add_time_to_params
    is_time_dependent_function
    orb_range
    EvaluatedSystem
