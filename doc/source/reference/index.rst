Core modules
============

The following modules of tkwant are used directly most frequently

.. toctree::
    :maxdepth: 1

    tkwant
    tkwant.leads
    tkwant.onebody
    tkwant.manybody

Modules mainly for internal use
===============================

The following modules contain functionality that is most often used only
internally by tkwant itself or by advanced users.

.. toctree::
    :maxdepth: 1

    tkwant.system
    tkwant.onebody.kernel
    tkwant.onebody.solvers
    tkwant.integrate
    tkwant.mpi
    tkwant.logging
