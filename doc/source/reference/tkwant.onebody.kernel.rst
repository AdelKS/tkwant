:mod:`tkwant.onebody.kernels` -- Evaluating the right hand side of the time-dependent Schrödinger equation
==========================================================================================================
This module is mainly for internal use by `tkwant.onebody.solvers`.

Kernels
-------
.. module:: tkwant.onebody.kernels
.. autosummary::
    :toctree: generated

    Scipy
    Simple
    SparseBlas
    PerturbationExtractor
    PerturbationInterpolator
