.. _examples:

More examples
=============


:ref:`closed_system`

:ref:`open_system`

:ref:`restarting`

:ref:`alternative_boundary_conditions`

:ref:`voltage_raise`

:ref:`graphene`
