.. _citation:

Citation
========

If you have used tkwant for work that has lead to a scientific publication, 
we would appreciate if you cite the publication which introduces tkwant:

T. Kloss, J. Weston, B. Gaury, B. Rossignol, C. Groth and X. Waintal,
Tkwant: a software package for time-dependent quantum transport,
`arXiv:2009.03132 [cond-mat.mes-hall]. <https://arxiv.org/abs/2009.03132>`_
