The tkwant authors can be reached at tkwant-authors@kwant-project.org.

The principal developers of tkwant are

* Joseph Weston (TU Delft, CEA Grenoble)
* Thomas Kloss (CEA Grenoble, HLRS)
* Christoph Groth (CEA Grenoble)
* Xavier Waintal (CEA Grenoble)

Contributors to tkwant include

* Benoît Gaury (CEA Grenoble)
* Benoît Rossignol (CEA Grenoble)
* Tatiane Pereira dos Santos (CEA Grenoble)
* Phillipp Reck (CEA Saclay)
* Adel Kara Slimane (CEA Saclay)
* Geneviève Fleury (CEA Saclay)
* Baptiste Anselme Martin (CEA Grenoble)
* Bas Nijholt (Microsoft)

(CEA = Commissariat à l'énergie atomique et aux énergies alternatives,
HLRS = Höchstleistungsrechenzentrum Stuttgart)
